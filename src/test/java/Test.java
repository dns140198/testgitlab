public class Test {

    public void commonMethod1(){
        System.out.println("Common 1");
    }

    public void commonMethodB1() {
        System.out.println("Common B1");
        System.out.println("Additional B1");

        System.out.println("B1 edited");
        System.out.println("B1 edited again");
        System.out.println("B1 edited once again");
    }

    public void commonMethodB2(){
        System.out.println("Common B2");
        System.out.println("Additional B2");

        System.out.println("B2 edited");
        System.out.println("B2 edited one again");
    }
}
